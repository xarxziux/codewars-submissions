fn number(bus_stops:&[(i32,i32)]) -> i32 {
  bus_stops.iter().map(|x: &(i32, i32)| x.0 - x.1).sum()
}
