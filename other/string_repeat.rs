fn repeat_str(src: &str, count: usize) -> String {
  vec![src; count].into_iter().collect()
}
