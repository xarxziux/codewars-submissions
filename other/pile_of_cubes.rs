fn find_nb(start_n: u64) -> i32 {
    let mut t: (u64, u64, i32) = (start_n, 1, 0);
    
    while t.2 == 0 {
        let p = t.1.pow (3);
        if t.0 < p {t.2 = -1}
        else if t.0 == p {t.2 = t.1 as i32}
        else {t = (t.0 - p,  t.1 + 1, 0)}
    }
    
    t.2
    
}
