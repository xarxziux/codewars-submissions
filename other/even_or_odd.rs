fn even_or_odd(i: i32) -> &'static str {
  match i % 2 == 0 {
    true => "Even",
    false => "Odd",
  }  
}

    4 days ago
    Refactor

fn even_or_odd(i: i32) -> &'static str {
  if i % 2 == 0 {
    return "Even";
  }
  "Odd"
}
