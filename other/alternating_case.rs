fn to_alternating_case(s: &str) -> String {
  s.chars().map(flip_case).collect::<String>()
}

fn flip_case (c: char) -> char {
  if c.is_lowercase() {return c.to_uppercase().next().unwrap()};
  if c.is_uppercase() {return c.to_lowercase().next().unwrap()};
  c
}
