fn t_to_u (char_in: char) -> char {
    match char_in {
      'T' => 'U',
      't' => 't',
      _ => char_in,
    }
}

fn dna_to_rna (dna: &str) -> String {
  dna.chars().map (t_to_u).collect()
}
