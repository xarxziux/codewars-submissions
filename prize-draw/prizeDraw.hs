module Codewars.G964.Rank where

import Data.Char (toUpper)
import Data.List (sortBy)

nameToRank :: String -> Int
nameToRank xs = length xs + (sum $ map (fromEnum . toUpper) xs)

commaToSpace :: Char -> Char
commaToSpace c
  | c == ',' = ' '
  | otherwise = c

commaSplit :: String -> [String]
commaSplit s = words $ map commaToSpace s

sortGT :: (String, Int) -> (String, Int) -> Ordering
sortGT (x1, x2) (y1, y2)
  | x2 /= y2 = compare y2 x2
  | otherwise = compare x1 y1

getNthRank :: [String] -> [Int] -> Int -> String -- String
getNthRank xs ns n = fst $ rs !! (n - 1)
  where rs = sortBy sortGT . zipWith (,) xs . zipWith (*) ns $ map nameToRank xs

rank :: String -> [Int] -> Int -> String
rank "" _ _ = "No participants"
rank s ns n
  | n > length ns = "Not enough participants"
  | otherwise = getNthRank (commaSplit s) ns n

-- let getNthRank (names: string[]) (we: int[]) (n: int) =
--   Seq.map nameToRank names
--     |> Seq.map2 (*) we
--     |> Seq.zip names
--     |> Seq.sortBy (fun (x, y) -> -y, x)
--     |> Seq.item (n - 1)
--     |> fst

-- let nthRank(st: string) (we: int[]) (n: int) =
--   let names = st.Split ','
--   match names with
--   | [|""|] -> "No participants"
--   | x when Seq.length x < n -> "Not enough participants"
--   | _ -> getNthRank names we n
