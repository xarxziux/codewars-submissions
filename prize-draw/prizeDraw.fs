open System

let nameToRank (s: string): int =
  s.ToUpper()
    |> Seq.fold (fun a c -> a + int c - 64) 0
    |> (+) s.Length

let getNthRank (names: string[]) (we: int[]) (n: int) =
  Seq.map nameToRank names
    |> Seq.map2 (*) we
    |> Seq.zip names
    |> Seq.sortBy (fun (x, y) -> -y, x)
    |> Seq.item (n - 1)
    |> fst

let nthRank(st: string) (we: int[]) (n: int) =
  let names = st.Split ','
  match names with
  | [|""|] -> "No participants"
  | x when Seq.length x < n -> "Not enough participants"
  | _ -> getNthRank names we n
