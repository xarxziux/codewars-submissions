(*
 * Tests
 *
 * "Hello, World!"
 * "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++.+++++++++++++++++++++++++++++.+++++++..+++.+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++.++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++.+++++++++++++++++++++++++++++++++++++++++++++++++++++++.++++++++++++++++++++++++.+++.++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++.++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++.+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++."
 *
 * "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
 * "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++.+.+.+.+.+.+.+.+.+.+.+.+.+.+.+.+.+.+.+.+.+.+.+.+.+."
*)

(*
 * Version 1
 * Hacky, but working
 *
 * Test Results:
 * Tests/Sample Tests
 * Completed in 3.815300ms
 * Tests/Alternate Character Tests
 * Completed in 0.308200ms
 * Tests/Fixed Tests
 * Completed in 2.265100ms
 * Tests/Random Tests
 * Completed in 125.867200ms
 *)
let rec processCode (d: int) (a: char list) (cs: char list): char list =
  match Seq.tryFindIndex ((=) '.') cs with
  | None -> a
  | Some x -> processCode ((x + d) % 256) (char ((x + d) % 256) :: a)  cs.[(x + 1) .. ]

let interpret (code: string): string =
  Seq.toList code
    |> List.filter (fun x -> x = '+' || x = '.')
    |> processCode 0 []
    |> List.rev
    |> Array.ofList
    |> System.String

(*
 * Version 2
 * Optimised for speed, but not very elegant  :(
 *
 * Test Results:
 * Tests/Sample Tests
 * Completed in 6.318200ms
 * Tests/Alternate Character Tests
 * Completed in 0.217500ms
 * Tests/Fixed Tests
 * Completed in 1.484800ms
 * Tests/Random Tests
 * Completed in 42.226300ms
 *)
let interpret (code: string): string =
  let (len: int) = String.length code
  let rec pcInt (ptr: int) (curr: int) (accum: char seq): char seq =
    match ptr = len with
    | true -> accum
    | false ->
      match code.[ptr] with
      | '+' -> pcInt (ptr + 1) ((curr + 1) % 256) accum
      | '.' ->
        char curr
        |> Seq.singleton
        |> Seq.append accum
        |> pcInt (ptr + 1) curr
      | _ -> pcInt (ptr + 1) curr accum
  pcInt 0 0 Seq.empty
    |> Array.ofSeq
    |> System.String

(*
 * Version 3
 * Uses a list and head :: tail pattern instead of a
 * character sequence.  Fastest solution yet!
 *
 * Test Results:
 * Tests/Sample Tests
 * Completed in 1.760400ms
 * Tests/Alternate Character Tests
 * Completed in 0.235500ms
 * Tests/Fixed Tests
 * Completed in 1.733700ms
 * Tests/Random Tests
 * Completed in 25.302300ms
 *)
 let interpret (code: string): string =
   let (len: int) = String.length code
   let rec pcInt (ptr: int) (curr: int): char list =
     match ptr = len with
     | true -> []
     | false ->
       match code.[ptr] with
       | '+' -> pcInt (ptr + 1) ((curr + 1) % 256)
       | '.' -> char curr :: pcInt (ptr + 1) curr
       | _ -> pcInt (ptr + 1) curr
   pcInt 0 0
     |> Array.ofList
     |> System.String
