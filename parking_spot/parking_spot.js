const findNearestCorral = arr => index => {

    const nearestCorralRecur = dist => (
    const nearestCorralRecur = dist => (

        (dist > arr.length)
            ? 0
            : (arr[index - dist] === 'CORRAL' ||
                arr[index + dist] === 'CORRAL')
            ? dist
            : nearestCorralRecur (dist + 1)

    );

    return nearestCorralRecur (1);

};

const stepCount = arr => index => (

    (index + findNearestCorral (arr)(index)) * 2

);

const getShortest = (p1, p2) => (

    ((p1.steps === null) || (p1.steps >= p2.steps))
        ? p2
        : p1

);

const isOpenSpot = (accum, next, index, arr) => (

    (next !== 'OPEN')
        ? accum
        : getShortest (accum, {
            index,
            steps: stepCount (arr)(index)
        })

);

const getShortestSteps = (accum, item) => (

    (accum.steps < item.steps)
        ? accum
        : steps

);

const bestParkingSpot = arr => (

    arr.reduce (isOpenSpot, {steps: null}).index

);

const assertEqual = (p1, p2) => {

    console.log (p1 === p2 ? true : p1 + ' is not equal to ' + p2);

};

const arr1 = ["STORE","OPEN","TAKEN","OPEN","CORRAL"];
const arr2 = ["STORE","OPEN","CORRAL"]
const arr3 = ["STORE","OPEN","TAKEN","OPEN","CORRAL"]
const arr4 = ["STORE","TAKEN","CORRAL","TAKEN","OPEN","CORRAL","OPEN"]
const arr5 = ["STORE", "TAKEN", "TAKEN", "CORRAL", "TAKEN", "OPEN", "OPEN", "TAKEN", "CORRAL"]
const arr6 = ["STORE","CORRAL","TAKEN","OPEN","TAKEN","TAKEN","OPEN","CORRAL","OPEN"]
const arr7 = ["STORE","OPEN","OPEN","OPEN","OPEN","CORRAL","OPEN"]
const arr8 = ["STORE","TAKEN","TAKEN","TAKEN","TAKEN","CORRAL","OPEN"]

console.log ('Testing findNearestCorral');
assertEqual (findNearestCorral (arr1)(1), 3);
assertEqual (findNearestCorral (arr2)(1), 1);
assertEqual (findNearestCorral (arr3)(3), 1);
assertEqual (findNearestCorral (arr4)(4), 1);
assertEqual (findNearestCorral (arr5)(6), 2);
assertEqual (findNearestCorral (arr6)(3), 2);
assertEqual (findNearestCorral (arr7)(3), 2);
assertEqual (findNearestCorral (arr8)(6), 1);

console.log ('Testing stepCount');
assertEqual (stepCount (arr1)(1), 8);
assertEqual (stepCount (arr2)(1), 4);
assertEqual (stepCount (arr3)(3), 8);
assertEqual (stepCount (arr4)(4), 10);
assertEqual (stepCount (arr5)(6), 16);
assertEqual (stepCount (arr6)(3), 10);
assertEqual (stepCount (arr7)(3), 10);
assertEqual (stepCount (arr8)(6), 14);

console.log ('Testing isOpenSpot');
console.log (arr1.reduce (isOpenSpot, {steps: null}));
console.log (arr2.reduce (isOpenSpot, {steps: null}));
console.log (arr3.reduce (isOpenSpot, {steps: null}));
console.log (arr4.reduce (isOpenSpot, {steps: null}));
console.log (arr5.reduce (isOpenSpot, {steps: null}));
console.log (arr6.reduce (isOpenSpot, {steps: null}));
console.log (arr7.reduce (isOpenSpot, {steps: null}));
console.log (arr8.reduce (isOpenSpot, {steps: null}));

console.log ('Testing bestParkingSpot');
assertEqual (bestParkingSpot (arr1), 3);
assertEqual (bestParkingSpot (arr2), 1);
assertEqual (bestParkingSpot (arr3), 3);
assertEqual (bestParkingSpot (arr4), 4);
assertEqual (bestParkingSpot (arr5), 5);
assertEqual (bestParkingSpot (arr6), 3);
assertEqual (bestParkingSpot (arr7), 4);
assertEqual (bestParkingSpot (arr8), 6);
