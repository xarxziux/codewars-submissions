from math import factorial

def pascal (row, elem):
	return factorial (row) / (factorial (elem) * factorial (row - elem))

def diagonal (x, y):
	return pascal (x + 1, y + 1)
