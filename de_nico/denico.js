const reverseMod = mod => val => (

    (mod - (val % mod)) % mod

);

const extendRight = len => str => (

    str + ' '.repeat(reverseMod (len)(str.length))

);

const encodeStr = str => code => (

    code.map ((x, i) => (

        x + str[i]

    ))

);

const splitStr = len => str => {

    const splitStrRecur = (str, accum) => (

        (str === '')
            ? accum
            : splitStrRecur (str.slice (len), encodeStr (str.slice (0, 7))(accum))

    );

    return splitStrRecur (extendRight (len)(str), Array (len).fill (''));

};
module.export = splitStr;
