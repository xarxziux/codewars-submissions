solution <- function(n){
  fb <- floor ((n - 1)/15)
  f <- floor ((n - 1)/3) - fb
  b <- floor ((n - 1)/5) - fb
  
  c(f, b, fb)
}