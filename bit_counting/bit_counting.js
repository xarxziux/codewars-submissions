const countBitsRecur = (x, bitCount) => (
    
    (x <= 0)
        ? bitCount
        : countBitsRecur (Math.floor (x / 2), bitCount + (x % 2))
    
);

const countBits = x => countBitsRecur (x, 0);

const countBits = x => x.toString(2).match(/1/g).length;
