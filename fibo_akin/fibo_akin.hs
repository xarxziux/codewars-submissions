module Codewars.G964.Fibkind where

import Data.Array

getFibo :: Integer -> Integer -> Array Integer -> Array Integer
getFibo l c a
    | c >= l = a
    | otherwise getFibo l (c + 1) a'
    where a' = a

let getFibo (limit: int): int[] =
  let rec gfInt (c: int) (a: int[]): int[] =
    match c >= limit with
      | true -> a
      | false ->
        gfInt
          (c + 1)
          (Array.append a [|
            a.[c - a.[c - 2]] + a.[c - a.[c - 1]]
          |])
  gfInt 2 [| 1; 1 |]

let lengthSupUK (n: int) (k: int): int =
  getFibo n
    |> Array.filter ((<=) k)
    |> Array.length

let countLower (x: int, a: int) (y: int): int * int =
  match y < x with
  | true -> (y, a + 1)
  | false -> (y, a)

let comp (n: int): int =
  getFibo n
    |> Array.fold countLower (0, 0)
    |> snd
