const gcd = (x, y) => (

    (x === y)
        ? x
        : x > y
            ? gcd (x - y, y)
            : gcd (x, y - x)

);

const lcm = (x, y) => (

    (x * y) / gcd (x, y)

);

const reduceFraction = x => {

    const gcdx = gcd (x[0], x[1]);

    return ([x[0]/gcdx, x[1]/gcdx]);

};

const sumFractions = (x, y) => {

    const denom = lcm (x[1], y[1]);

    return [((x[0] * denom) / x[1]) + ((y[0] * denom) / y[1]),
        denom];

};

const reduceWholeNumber = x => (

    (x[1] === 1)
        ? x[0]
        : x

);

const sumFracts = arr => (

    reduceWholeNumber (
        reduceFraction (
            arr.reduce (sumFractions)))

);

module.exports = sumFracts;
