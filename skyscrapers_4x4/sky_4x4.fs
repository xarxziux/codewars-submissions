type block = int * int * int * int
type grid = block * block * block * block
type ends = int * int

let first  (a, _, _, _) = a
let second (_, a, _, _) = a
let third  (_, _, a, _) = a
let fourth (_, _, _, a) = a
let getRow1 (g: grid): block = first g
let getRow2 (g: grid): block = second g
let getRow3 (g: grid): block = third g
let getRow4 (g: grid): block = fourth g

let getCol1 (g: grid): block =
  let (a, b, c, d) = g
  (first a, first b, first c, first d)
let getCol2 (g: grid): block =
  let (a, b, c, d) = g
  (second a, second b, second c, second d)
let getCol3 (g: grid): block =
  let (a, b, c, d) = g
  (third a, third b, third c, third d)
let getCol4 (g: grid): block =
  let (a, b, c, d) = g
  (fourth a, fourth b, fourth c, fourth d)

(*
let blockToArr (bl: block): int[] =
  let (a, b, c, d) = bl
  [| a; b; c; d |]
*)

let noDups (bl: block): bool =
    let (a, b, c, d) = bl
    let blockFilter = Array.filter (fun x -> x <> 0) [| a; b; c; d |]
    blockFilter
        |> Set.ofArray
        |> Set.count
        |> (=) blockFilter.Length

let isValidGrid (g: grid): bool =
    noDups (getRow1 g) &&
    noDups (getRow2 g) &&
    noDups (getRow3 g) &&
    noDups (getRow4 g) &&
    noDups (getCol1 g) &&
    noDups (getCol2 g) &&
    noDups (getCol3 g) &&
    noDups (getCol4 g)
