const sumTwo = (x, y) => x + y;

const lastSix = arr => index => (

    ((index < 0) || (index > arr.length + 4))
        ? 0
        : arr.slice (Math.max (0, index - 5), Math.min (arr.length, index + 1))
            .reduce (sumTwo)

);

const nextLevel = arr => {

    const getNext = lastSix (arr);

    const nextLevelRecur = (index, next, accum) => (

        next === 0
            ? accum
            : nextLevelRecur (index + 1, getNext (index), accum.concat (next))

    );

    return nextLevelRecur (1, getNext (0), []);

};

const getLevel = (x, accum = [1]) => (

    x === 0
        ? accum
        : getLevel (x - 1, nextLevel (accum))

);

const rolldiceSumProb = (target, totalSides) => {

    if ((target < totalSides) || (target > totalSides * 6))
        return 0;

    const levelArr = getLevel (totalSides);

    return levelArr [target - totalSides] / levelArr.reduce (sumTwo);

};

module.export = rolldiceSumProb;
