fn get_last_six (base_vec: Vec<u32>) -> Box<Fn(i32) -> u32> {
    
    
    Box::new (move |i: i32| base_vec [
        // (0.max (i - 6).min (base_vec.len() as i32) as usize)..
        // ((base_vec.len()).min (i as usize))
        (std::cmp::min (
            std::cmp::max (0, i - 6), base_vec.len() as i32) as usize)..
        (std::cmp::min (base_vec.len(), i as usize))
    ].into_iter().sum::<u32>())
    
}

fn get_next_level (last_level: Vec<u32>) -> Vec<u32> {
    
    let last_six = get_last_six (last_level);
    let mut index: i32 = 1;
    let mut next_value: u32 = last_six (index);
    let mut next_level: Vec<u32> = Vec::new();
    
    while next_value != 0 {
        
        next_level.push (next_value);
        index = index + 1;
        next_value = last_six (index);
        
    }
    
    next_level
    
}

fn get_level (target_level: u32) -> Vec<u32> {
    
    let mut target: u32 = target_level;
    let mut level_x: Vec<u32> = vec!(1);
    
    while target > 0 {
        
        level_x = get_next_level (level_x.clone());
        target = target - 1;
        
    }
    
    level_x
    
}

fn get_prob (target: usize, dice_count: usize) -> f64 {
    
    let dice_level: Vec<u32> = get_level (dice_count as u32);
    let num: f64 = dice_level [target - dice_count] as f64;
    let denom: u32 =  dice_level.into_iter().sum();
    
    num / denom as f64
    
}

fn rolldice_sum_prob (target: usize, dice_count: usize) -> f64 {
    
    if (target < dice_count) || (target > dice_count * 6) {
        
        0.0
        
    } else {
        
        get_prob (target, dice_count)
        
    }
    
}
