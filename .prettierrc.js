module.exports  = {

    printWidth: 78,
    tabWidth: 4,
    useTabs: false,
    semi: true,
    singleQuote: true,
    trailingComma: 'none',
    bracketSpacing: false,
    jsxBracketSameLine: false,
    arrowParens: 'avoid',
    rangeStart: 0,
    rangeEnd: Infinity,
    requirePragma: true,
    insertPragma: true,
    proseWrap: 'preserve'

};
