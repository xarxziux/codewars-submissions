#lang racket

(provide count-kprimes)

(define (prime-count base [div 2] [accum 1])
  (cond
    [(> (* div 2) base) accum]
    [(= 0 (remainder base div))
     (prime-count (/ base div) div (+ accum 1))]
    [else (prime-count base (if (= div 2) 3 (+ div 2)) accum)]))
  
(define (count-kprimes k start end)
  (for/list
      ([x (range start (+ 1 end))]
       #:when (= k (prime-count x)))
    x))

(define-syntax-rule (get-end . x) (set! x (last x)))

(define-syntax-rule (swap x y)
  (let ([tmp x])
    (set! x y)
    (set! y tmp)))
