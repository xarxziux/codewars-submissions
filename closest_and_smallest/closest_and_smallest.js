const weight = num => (

    num < 10
        ? num
        : (num % 10) + weight (Math.floor (num / 10))

);

const parseString = str => (

    str.split (' ').map (x => x - 0)

);

const mapElement = (current, index) => ([

    weight (current),
    index,
    current

]);

const orderByWeight = arr => arr.slice().sort ((x, y) => x[0] - y[0]);

const groupElements = (arr, index = 1) => {

    console.log ('arr = ', arr, ', index = ', index);

    return (arr.length === 2
        ? [[arr[0], arr[1]]]
        : index === arr.length
        ? groupElements (arr.slice (1), 1)
        : [[arr[0], arr[index]]].concat (groupElements (arr, index + 1)));

};
