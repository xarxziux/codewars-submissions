const strToLen = str => str.length;

const sumArray = arr => arr.reduce ((i, n) => i + n);

const sumXFromN = x => arr => n => sumArray (arr.slice (n, n + x));

const findLongestSequence = l => arr => {

    const endIndex = arr.length - l;
    const sumLFromN = sumXFromN (l)(arr);

    const findLongestRecur = (longestIndex, currIndex, score) => {

        if (currIndex > endIndex)
            return longestIndex;

        const newScore = sumLFromN (currIndex);

        if (newScore > score)
            return findLongestRecur (currIndex, currIndex + 1, newScore);

        return findLongestRecur (longestIndex, currIndex + 1, score);

    };

    return findLongestRecur (0, 1, sumLFromN (0));

};

function longestConsec(strarr, k) {

    const n = strarr.length;

    if ((n === 0) || (k <= 0) || (k > n))
        return '';

    const i = findLongestSequence (k)(strarr.map (strToLen));

    return strarr.slice (i, i + k).join ('');
}

function test (actual, expected) {

    if (actual === expected)
        console.log ('Test passed!');
    else
        console.log ('Test failed: ', expected, 'expected,', actual, 'returned.');

}

test (longestConsec(['zone', 'abigail', 'theta', 'form', 'libe', 'zas'], 2), 'abigailtheta');
test (longestConsec(['ejjjjmmtthh', 'zxxuueeg', 'aanlljrrrxx', 'dqqqaaabbb', 'oocccffuucccjjjkkkjyyyeehh'], 1), 'oocccffuucccjjjkkkjyyyeehh');
test (longestConsec([], 3), '');
test (longestConsec(['itvayloxrp','wkppqsztdkmvcuwvereiupccauycnjutlv','vweqilsfytihvrzlaodfixoyxvyuyvgpck'], 2), 'wkppqsztdkmvcuwvereiupccauycnjutlvvweqilsfytihvrzlaodfixoyxvyuyvgpck');
test (longestConsec(['wlwsasphmxx','owiaxujylentrklctozmymu','wpgozvxxiu'], 2), 'wlwsasphmxxowiaxujylentrklctozmymu');
test (longestConsec(['zone', 'abigail', 'theta', 'form', 'libe', 'zas'], -2), '');
test (longestConsec(['it','wkppv','ixoyx', '3452', 'zzzzzzzzzzzz'], 3), 'ixoyx3452zzzzzzzzzzzz');
test (longestConsec(['it','wkppv','ixoyx', '3452', 'zzzzzzzzzzzz'], 15), '');
test (longestConsec(['it','wkppv','ixoyx', '3452', 'zzzzzzzzzzzz'], 0), '');
test (longestConsec(['zone', 'abigail', 'theta', 'form', 'libe', 'zas'], 2), 'abigailtheta');
test (longestConsec(['ejjjjmmtthh', 'zxxuueeg', 'aanlljrrrxx', 'dqqqaaabbb', 'oocccffuucccjjjkkkjyyyeehh'], 1), 'oocccffuucccjjjkkkjyyyeehh');
test (longestConsec([], 3), '');
test (longestConsec(['itvayloxrp','wkppqsztdkmvcuwvereiupccauycnjutlv','vweqilsfytihvrzlaodfixoyxvyuyvgpck'], 2), 'wkppqsztdkmvcuwvereiupccauycnjutlvvweqilsfytihvrzlaodfixoyxvyuyvgpck');
test (longestConsec(['wlwsasphmxx','owiaxujylentrklctozmymu','wpgozvxxiu'], 2), 'wlwsasphmxxowiaxujylentrklctozmymu');
test (longestConsec(['zone', 'abigail', 'theta', 'form', 'libe', 'zas'], -2), '');
test (longestConsec(['it','wkppv','ixoyx', '3452', 'zzzzzzzzzzzz'], 3), 'ixoyx3452zzzzzzzzzzzz');
test (longestConsec(['it','wkppv','ixoyx', '3452', 'zzzzzzzzzzzz'], 15), '');
test (longestConsec(['it','wkppv','ixoyx', '3452', 'zzzzzzzzzzzz'], 0), '');
