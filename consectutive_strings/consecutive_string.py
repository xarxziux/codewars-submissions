def longest_consec (strarr, k):
    if ((len (strarr) == 0) or ( k <= 0) or (k > len (strarr))):
        return ''
    else:
        return findLongestSequence (strarr, k)

def findLongestSequence (arr, k):
    def findLongestRecur (slicedArr, longest):
    def findLongestRecur (slicedArr, longest):
        if (len (slicedArr) < k):
            return longest
        else:
            return findLongestRecur (
                slicedArr[1:],
                getLongest (longest, ''.join(slicedArr[:k])))
    return findLongestRecur (arr[1:], ''.join(arr[:k]))

def getLongest (currentStr, nextStr):
    if (len (currentStr) >= len (nextStr)):
        return currentStr
    else:
        return nextStr
