const accum = s => (

    s.split ('')
        .map ((char, len) => (
            char.toUpperCase() + char.toLowerCase().repeat (len)))
        .join ('-')

);

module.exports = accum;
