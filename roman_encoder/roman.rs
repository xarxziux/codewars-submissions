fn num_as_roman (dec: usize) -> String {
    
    let units_to_roman = num_to_roman ('I', 'V', 'X');
    let tens_to_roman = num_to_roman ('X', 'L', 'C');
    let hundreds_to_roman = num_to_roman ('C', 'D', 'M');
    
    let units = units_to_roman [dec % 10].clone();
    let tens = tens_to_roman [(dec / 10) % 10].clone();
    let hundreds = hundreds_to_roman [(dec / 100) % 10].clone();
    let mut thousands = thousands_to_roman ((dec / 1000) % 10).clone();
    
    thousands.push_str (&hundreds);
    thousands.push_str (&tens);
    thousands.push_str (&units);
    thousands.clone()
    
}

fn add_two_chars (x: char, y: char) -> String {

    let mut ret = String::new();

    ret.push (x);
    ret.push (y);
    ret
    
}

fn add_three_chars (x: char, y: char, z: char) -> String {

    let mut ret = String::new();

    ret.push (x);
    ret.push (y);
    ret.push (z);
    ret
    
}

fn add_four_chars (x: char, y: char, z: char, aa: char) -> String {

    let mut ret = String::new();

    ret.push (x);
    ret.push (y);
    ret.push (z);
    ret.push (aa);
    ret
    
}

fn num_to_roman (base: char, mid: char, top: char) -> [String; 10] {[
    
    String::new(),
    base.to_string(),
    add_two_chars (base, base),
    add_three_chars (base, base, base),
    add_two_chars (base, mid),
    mid.to_string(),
    add_two_chars (mid, base),
    add_three_chars (mid, base, base),
    add_four_chars (mid, base, base, base),
    add_two_chars (base, top)
    
]}

fn thousands_to_roman (x: usize) -> String {
    
    match x {
        0 => String::new(),
        1 => String::from ("M"),
        2 => String::from ("MM"),
        3 => String::from ("MMM"),
        4 => String::from ("MMMM"),
        _ => String::from ("MMMMM")
    }
    
}
