def numToRoman (base, mid, top):
    return ['',
            base,
            base + base,
            base + base + base,
            base + mid,
            mid,
            mid + base,
            mid + base + base,
            mid + base + base + base,
            base + top]

unitsToRoman = numToRoman ('I', 'V', 'X')
tensToRoman = numToRoman ('X', 'L', 'C')
hundredsToRoman = numToRoman ('C', 'D', 'M')

def thousandsToRoman (x):
    return 'M' * x

def strToRoman (str):
    return (thousandsToRoman (int (str[0])) +
            hundredsToRoman [int (str[1])] +
            tensToRoman [int (str[2])] +
            unitsToRoman [int (str[3])])

def padStr (str):
    return '0' * (4 - len (str)) + str

def solution (decimal):
    return strToRoman (padStr (str (decimal)))
