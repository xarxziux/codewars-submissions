const numToRoman = ({base, mid, top}) => ([
    '',
    base,
    base + base,
    base + base + base,
    base + mid,
    mid,
    mid + base,
    mid + base + base,
    mid + base + base + base,
    base + top
]);

const unitsToRoman = numToRoman ({base: 'I', mid: 'V', top: 'X'});
const tensToRoman = numToRoman ({base: 'X', mid: 'L', top: 'C'});
const hundredsToRoman = numToRoman ({base: 'C', mid: 'D', top: 'M'});
const thousandsToRoman = x => 'M'.repeat (x);

const strToRoman = str => (

    thousandsToRoman (str[0]) +
        hundredsToRoman [str[1]] +
        tensToRoman [str[2]] +
        unitsToRoman [str[3]]

);

const padStr = str => (

    '0'.repeat (4 - str.length) + str

);

const solution = decimal => (

    strToRoman (padStr ('' + decimal))

);

module.exports = solution;
