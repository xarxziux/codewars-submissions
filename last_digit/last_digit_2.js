const trace = (x, message = '') => {

    console.log (message);
    return x;

};

const twoPattern = [6, 2, 4, 8];
const threePattern = [1, 3, 9, 7];
const sevenPattern = [1, 7, 9, 3];
const eightPattern = [6, 8, 4, 2];

const raiseByTwo = power => trace (twoPattern [power % 4], 'raiseByTwo fired');
const raiseByThree = power => trace (threePattern [power % 4], 'raiseByThree fired');
const raiseBySeven = power => trace (sevenPattern [power % 4], 'raiseBySeven fired');
const raiseByEight = power => trace (eightPattern [power % 4], 'raiseByEight fired');

const justZero = () => trace (0, 'justZero fired');
const justOne = () => trace (1, 'justOne fired');
const justFive = () => trace (5, 'justFive fired');
const justSix = () => trace (6, 'justSix fired');

const raiseByFour = x => (
    trace (x % 2 === 1
        ? 4
        : 6, 'raiseByFour fired')
);

const raiseByNine = x => (
    trace (x % 2 === 1
        ? 9
        : 1, 'raiseByNine fired')
);

const powerGrid = [
    justZero,
    justOne,
    raiseByTwo,
    raiseByThree,
    raiseByFour,
    justFive,
    justSix,
    raiseBySeven,
    raiseByEight,
    raiseByNine
];

const raiseBy = (power, next) => {

    console.log ('next = ' + next + ', power = ' + power);

    return (power === 0
        ? 1
        : powerGrid [next % 20](power))

};

let rightMod = mod => x => {

    const modX = x % mod;

    return (modX === 0
        ? mod
        : modX)

};

let rightMod20 = rightMod (20);
let rightMod100 = rightMod (100);

let raiseBy = (power, base, accum = base) => {

    return (power === 1
        ? accum
        : raiseBy (power - 1, base, rightMod100 (accum * base)));

};

let raiseByNonZero = (power, base) => (

    power === 0
        ? 1
        : raiseBy (power % 20, base)

);

let raiseByArr = arr => (

    arr.length === 0
        ? 0
        : arr.reduceRight (raiseByNonZero)

);

let rightMod = mod => x => {

    const modX = x % mod;

    return (modX === 0
        ? mod
        : modX);

};

let rightMod100 = rightMod (100);

let raiseBy2 = (power, base, accum = base) => {

    return (power === 1
        ? accum
        : raiseBy2 (power - 1, base, rightMod100 (accum * base)));

};

let raiseByNonZero = (power, base) => (

    power === 0
        ? 1
        : raiseBy (power % 20, base)

);

let raiseByArr = arr => (

    arr.length === 0
        ? 0
        : arr.reduceRight (raiseByNonZero)

);

module.exports = raiseByArr;
