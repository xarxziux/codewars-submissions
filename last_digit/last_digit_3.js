const setMax = max => x => (

    x < max
        ? x
        : max + (x % max)

);

const max20 = setMax (20);
const max4 = setMax (4);

const raiseLimitedByX = (pow, base) => (

    pow === 0
        ? 1
        : max20 (base * (max20 (raiseLimitedByX (pow - 1, base))))

);

const raiseByX = (pow, base) =>
    raiseLimitedByX (max4 (pow), max20 (base));

const getLast = x => x % 10;

const lastDigit = arr => getLast (arr.reduceRight (raiseByX, 1));

module.exports = lastDigit;
