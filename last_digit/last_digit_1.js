const raiseBy = (power, next) => {

    return Math.pow (next % 100, power) % 20;

};

const lastDigit = arr => (

    arr.length === 0
        ? 1
        : arr.reduceRight (raiseBy) % 10

);

module.exports = lastDigit;
