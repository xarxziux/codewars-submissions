fn max_20 (x: u64) -> u64 {
    
    if x < 20 { x }
    else { 20 + x % 20 }
    
}

fn max_4 (x: u64) -> u64 {
    
    if x < 4 { x }
    else { 4 + x % 4 }
    
}

fn raiseBy (pow: u64, base: u64) -> u64 {
    
    u64::pow (max_20 (base), max_4 (pow))
    
}


fn last_digit (arr: &[u64]) -> u64 {
    
    arr.iter().reduceRight (raiseBy) % 10
    
}
