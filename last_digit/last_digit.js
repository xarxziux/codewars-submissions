const trace = (x, message = '') => {
    
    console.log (message);
    return x;
    
};

const twoPattern = [6, 2, 4, 8];
const threePattern = [1, 3, 9, 7];
const sevenPattern = [1, 7, 9, 3];
const eightPattern = [6, 8, 4, 2];

const raiseByTwo = power => trace (twoPattern [power % 4], 'raiseByTwo fired');
const raiseByThree = power => trace (threePattern [power % 4], 'raiseByThree fired');
const raiseBySeven = power => trace (sevenPattern [power % 4], 'raiseBySeven fired');
const raiseByEight = power => trace (eightPattern [power % 4], 'raiseByEight fired');

const justZero = () => trace (0, 'justZero fired');
const justOne = () => trace (1, 'justOne fired');
const justFive = () => trace (5, 'justFive fired');
const justSix = () => trace (6, 'justSix fired');

const raiseByFour = x => (
    trace (x % 2 === 1
        ? 4
        : 6, 'raiseByFour fired')
);

const raiseByNine = x => (
    trace (x % 2 === 1
        ? 9
        : 1, 'raiseByNine fired')
);

const powerGrid = [
    justZero,
    justOne,
    raiseByTwo,
    raiseByThree,
    raiseByFour,
    justFive,
    justSix,
    raiseBySeven,
    raiseByEight,
    raiseByNine
];

const raiseBy = (power, next) => {
    
    console.log ('next = ' + next + ', power = ' + power);
    
    return (power === 0
        ? 1
        : powerGrid [next % 10](power))
    
};

const lastDigit = arr => (
    
    arr.length === 0
        ? 1
        : arr.reduceRight (r    aiseBy)
    
);

console.log (lastDigit([]));
console.log ();
console.log (lastDigit([0,0]));
console.log ();
console.log (lastDigit([0,0,0]));
console.log ();
console.log (lastDigit([1,2]));
console.log ();
console.log (lastDigit([3,4,5]));
console.log ();
console.log (lastDigit([4,3,6]));
console.log ();
console.log (lastDigit([7,6,21]));
console.log ();
console.log (lastDigit([12,30,21]));
console.log ();
console.log (lastDigit([2,2,2,0]));
console.log ();
console.log (lastDigit([937640,767456,981242]));
console.log ();
console.log (lastDigit([123232,694022,140249]));
console.log ();
console.log (lastDigit([499942,898102,846073]));
console.log ();
