def max20 (x):
    if (x < 20):
        return
    return 20 + (x % 20)

def raiseBy (pow, base):
    (max20 (base)) ** (max20 (pow))
