module Codewars.Kata.Which where

import Data.List (isInfixOf, sort)

inArray :: [[Char]] -> [[Char]] -> [[Char]]
inArray x y = sort (isAllIn [] x y)

isIn :: [Char] -> [[Char]] -> Bool
isIn e [] = False
isIn e (x:xs) =
  if isInfixOf e x
    then True
    else isIn e xs

isAllIn :: [[Char]] -> [[Char]] -> [[Char]] -> [[Char]]
isAllIn accum [] y = accum
isAllIn accum (x:xs) y =
  if not (isIn x y)
    then isAllIn accum xs y
  else if elem x accum
    then isAllIn accum xs y
    else isAllIn (x:accum) xs y
