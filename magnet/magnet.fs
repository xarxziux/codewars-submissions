let magPower (k: int) (n: int): float =
  let k64 = uint64 k
  let n64 = uint64 n
  try
    1.0 / (float ((k64 * (pown (n64 + 1UL) (k * 2)))))
  with
    | :? System.OverflowException -> 0.0

let sumRow (k: int) (n: int): float =
  let rec srInt (n': int) (a: float): float =
    printfn "%i" n'
    printfn "%f" a
    match n' > n with
      | true -> a
      | false ->
        match magPower k n' with
          | x when x = 0.0 -> a
          | y -> srInt (n' + 1) (a + y)
  srInt 1 0.0

let doubles (k: int) (n: int): float =
  [1 .. k]
    |> Seq.fold (fun x y -> x + sumRow y n) 0.0
