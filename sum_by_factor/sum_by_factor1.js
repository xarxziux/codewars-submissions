/*
    Type definition for accumulator Object
    accum = {
        original: integer[],
        reduced: integer[],
        factors: [[prime, integer]],
        divisor: integer,
        index: integer
    }
*/

// Basic functions
const isEmpty = arr => (arr.length === 0);
const getHead = arr => arr [0];
const loseHead = arr => arr.slice (1);
const getEnd = arr => arr [arr.length - 1];
const loseEnd = arr => arr.slice (0, arr.length - 1);
const isNumber = x => ((typeof x === 'number') &&
    (x === x) &&
    (isFinite (x)));

const isAccum = x => (

    (typeof x === 'object') &&
        (Array.isArray (x.original)) &&
        (Array.isArray (x.reduced)) &&
        (Array.isArray (x.factors)) &&
        ((isEmpty (x.factors)) ||
            (Array.isArray (x.factors [0]))) &&
        (isNumber (x.divisor)) &&
        (isNumber (x.index))

);

const getOriginal = ({original, index}) => original [original.length - index];
const getReduced = ({reduced, index}) => reduced [reduced.length - index];
const getLastFactor = ({factors}) => getEnd (factors)[0];
const getLastFactorSum = ({factors}) => getEnd (factors)[1];

const absMax = (x, y) => (

    Math.max (Math.abs (x), Math.abs (y))

);

const getMaxFactor = arr => (

    Math.ceil (
        Math.sqrt (
            arr.reduce (absMax)))

);

const makeOdd = x => (

    x + 1 - (x % 2)

);

const buildFactorArr = maxFactor => {

    const buildFactorArrRecur = ({currMax, accum}) => (

        (currMax === 3)
            ? [2, 3].concat (accum)
            : buildFactorArrRecur ({
                currMax: currMax - 2,
                accum: [currMax].concat (accum)
            })

    );

    if (maxFactor === 2)
        return [2];

    return buildFactorArrRecur ({
        currMax: makeOdd (maxFactor),
        accum: []
    });

};

const addFactor = accum => (

    Object.assign ({}, accum, {

        factors: accum.factors.concat ([[
            accum.divisor,
            getOriginal (accum)
        ]])

    })

);

const incrementFactor = accum => (

    Object.assign ({}, accum, {

        factors: loseEnd (accum.factors).concat ([[
            getLastFactor (accum),
            getLastFactorSum (accum) + getOriginal (accum)
        ]])

    })

);

const updateFactorArr = accum => (

    (getLastFactor (accum) === accum.divisor)
        ? incrementFactor (accum)
        : addFactor (accum)

);

const reduceBy = ({divisor, base}) => (

    (base % divisor !== 0)
        ? base
        : reduceBy ({
            divisor,
            base: base / divisor
        })

);

const incIndex = accum => (

    Object.assign ({}, accum, {
        index: accum.index - 1
    })

);

const updateReducedArr = (accum, nextDivisor) => {

    if (accum.index === 0)
        return accum;

    const currReduced = getReduced (accum);
    const newReduced = reduceBy ({nextDivisor, base: currReduced});

    if (currReduced === newReduced)
        return incIndex (accum);

    return incIndex (
        updateFactorArr (accum));

};

const main = arr => {

    buildFactorArr (
        getMaxFactor (arr))
        .reduce (updateReducedArr, {
            original: arr,
            reduced: arr,
            factors: [],
            index: arr.length
        })
        .factors;

};

/*
const updateReducedArr = ({reduced, newTail}) => (

    (newTail === 1)
        ? reduced
        : reduced.concat (newTail)

);
*/

/*
const reduceArrBy = base => {

    const reduceByDivisor = reduceBy (accum.divisor);

    const reduceByRecur = accum => {

        if (isEmpty (accum.target))
            return accum;

        const head = getHead (accum.target);
        const newHead = reduceByDivisor (head);
        const isReduced = (head === newHead);

        return ({

            target: getTail (accum.target),
            reduced: updateReducedArr ({
                reduced: accum.reduced,
                newTail: newHead
            }),
            divisor: accum.divisor,
            factors: updateFactorArr ({
                factor: accum.divisor,
                factorArr: accum.factorArr,
                isReduced
            })

        });


    };

};
*/

/*
const reduceNumberBy = accum => {

    const reduceHeadByDivisor = reduceHeadBy (accum.divisor);

    const reduceByRecur = accum => (

        (accum.base[0] % accum.divisor !== 0)
            ? accum
            : reduceByRecur (Object.assign ({}, accum, {
                base: reduceHeadByDivisor (accum.base),
                factors:
            })

    );

    return reduceByRecur (base);

};
*/

/*
const reduceAccumBy = accum => {



};
*/

/*
const reduceArrBy = (accum, divisor) => {

    const reduceByDivisor = reduceBy (divisor);



};
*/

module.exports = {

    isEmpty,
    getHead,
    loseHead,
    getEnd,
    loseEnd,
    reduceBy,
    isNumber,
    isAccum,
    addFactor,
    incrementFactor,
    updateFactorArr,
    getOriginal,
    getReduced,
    getLastFactor,
    getLastFactorSum,
    incIndex,
    main

};
