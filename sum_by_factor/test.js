const test = require ('tape');
const _main = require ('./sum_by_factor.js');
const main = (typeof _main === 'object'
    ? _main
    : {}
);

const failFn = () => '__fail__';

const getFn = fn => (

    (typeof main [fn] === 'function')
        ? main [fn]
        : failFn

);

const isEmpty = getFn ('isEmpty');
const getHead = getFn ('getHead');
const loseHead = getFn ('loseHead');
const getEnd = getFn ('getEnd');
const loseEnd = getFn ('loseEnd');
const reduceBy = getFn ('reduceBy');
const isNumber = getFn ('isNumber');
const isAccum = getFn ('isAccum');
const getOriginal = getFn ('getOriginal');
const getReduced = getFn ('getReduced');
const getLastFactor = getFn ('getLastFactor');
const getLastFactorSum = getFn ('getLastFactorSum');
const addFactor = getFn ('addFactor');
const incrementFactor = getFn ('incrementFactor');
const updateFactorArr = getFn ('updateFactorArr');
const incIndex = getFn ('incIndex');

test ('Test main module', assert => {

    assert.plan (12);

    assert.equal (typeof _main, 'object',
        'The main module returns an object and...');
    assert.equal (typeof main ['isEmpty'], 'function',
        'that object contains an isEmpty function and...');
    assert.equal (typeof main ['getHead'], 'function',
        'that object contains a getHead function and...');
    assert.equal (typeof main ['loseHead'], 'function',
        'that object contains a loseHead function and...');
    assert.equal (typeof main ['getEnd'], 'function',
        'that object contains a getEnd function and...');
    assert.equal (typeof main ['loseEnd'], 'function',
        'that object contains a loseEnd function and...');
    assert.equal (typeof main ['reduceBy'], 'function',
        'that object contains a reduceBy function and...');
    assert.equal (typeof main ['isNumber'], 'function',
        'that object contains a isNumber function and...');
    assert.equal (typeof main ['isAccum'], 'function',
        'that object contains a isAccum function and...');
    assert.equal (typeof main ['addFactor'], 'function',
        'that object contains a addFactor function and...');
    assert.equal (typeof main ['incrementFactor'], 'function',
        'that object contains a incrementFactor function and...');
    assert.equal (typeof main ['updateFactorArr'], 'function',
        'that object contains a updateFactorArr function and...');
    //assert.equal (typeof main ['XXX'], 'function',
    //    'that object contains a XXX function and...');
    //assert.equal (typeof main ['XXX'], 'function',
    //    'that object contains a XXX function and...');
    //assert.equal (typeof main ['XXX'], 'function',
    //    'that object contains a XXX function and...');

    assert.end();

});

test ('Test basic functions', assert => {


    assert.plan (35);

    assert.equal (isEmpty ([]), true);
    assert.equal (isEmpty ([1]), false);
    assert.equal (isEmpty (['e', 'f', 'g']), false);
    assert.equal (isEmpty ([null]), false);
    assert.equal (isEmpty ([[]]), false);

    assert.equal (getHead ([1, 2, 3, 4, 5]), 1);
    assert.equal (getHead (['a', 'b', 'c']), 'a');
    assert.equal (getHead ([null, null, null]), null);
    assert.deepEqual (getHead ([{x: 7, y: 10}, {x: 5, y: 12}]),
        {x: 7, y: 10});
    assert.deepEqual (getHead ([[]]), []);

    assert.deepEqual (loseHead ([1, 2, 3, 4, 5]), [2, 3, 4, 5]);
    assert.deepEqual (loseHead (['a', 'b', 'c', 'd', 'e']),
        ['b', 'c', 'd', 'e']);
    assert.deepEqual (loseHead (['a']), []);
    assert.deepEqual (loseHead ([1, 2]), [2]);
    assert.deepEqual (loseHead ([{x: 7, y: 10}, {x: 5, y: 12}]),
        [{x: 5, y: 12}]);

    assert.equal (getEnd ([1, 2, 3, 4, 5]), 5);
    assert.equal (getEnd (['a', 'b', 'c', 'd', 'e']), 'e');
    assert.equal (getEnd ([1]), 1);
    assert.deepEqual (getEnd ([{x: 7, y: 10}, {x: 5, y: 12}]),
        {x: 5, y: 12});
    assert.deepEqual (getEnd ([[1], [2]]), [2]);

    assert.deepEqual (loseEnd ([1, 2, 3, 4, 5]), [1, 2, 3, 4]);
    assert.deepEqual (loseEnd (['a', 'b', 'c', 'd', 'e']),
        ['a', 'b', 'c', 'd']);
    assert.deepEqual (loseEnd (['a']), []);
    assert.deepEqual (loseEnd ([1, 2]), [1]);
    assert.deepEqual (loseEnd ([{x: 5, y: 12}, {x: 7, y: 10}]),
        [{x: 5, y: 12}]);

    assert.equal (reduceBy ({divisor: 2, base: 128}), 1);
    assert.equal (reduceBy ({divisor: 5, base: 625}), 1);
    assert.equal (reduceBy ({divisor: 2, base: 1001}), 1001);
    assert.equal (reduceBy ({divisor: 9, base: 729}), 1);
    assert.equal (reduceBy ({divisor: 3, base: 32805}), 5);

    assert.equal (isNumber (1), true);
    assert.equal (isNumber ('1'), false);
    assert.equal (isNumber (Number (1)), true);
    assert.equal (isNumber (NaN), false);
    assert.equal (isNumber (Math.PI), true);

    assert.end ();

});

const accum1 = {

    original: [114, 115, 116, 117, 118, 119, 120],
    reduced: [14, 15, 16, 17, 18, 19, 20],
    factors: [[2, 12], [3, 15], [7, 22], [11, 15]],
    divisor: 2,
    index: 4

};

test ('Test accum-reading functions', assert => {

    assert.plan (4);

    assert.equal (getOriginal (accum1), 117);
    assert.equal (getReduced (accum1), 17);
    assert.equal (getLastFactor (accum1), 11);
    assert.equal (getLastFactorSum (accum1), 15);

    assert.end();

});

test ('Accum update functions', assert => {

    const accum2 = incrementFactor (accum1);
    const accum3 = addFactor (accum2);
    const accum4 = incIndex (accum3);

    assert.plan (8);

    assert.equal (accum1.original, accum4.original);
    assert.equal (accum1.reduced, accum4.reduced);
    assert.equal (accum4.divisor, 2);
    assert.equal (accum4.index, 3);
    assert.equal (accum4.factors.length, 5);
    assert.deepEqual (accum4.factors.slice (-2), [[11, 132], [2, 117]]);
    assert.deepEqual (accum2.factors,
        [[2, 12], [3, 15], [7, 22], [11, 132]]);
    assert.deepEqual (accum4.factors,
        [[2, 12], [3, 15], [7, 22], [11, 132], [2, 117]]);

    assert.end();

});

/*
const {baseArr, factorArr} = reduceArrBy7 (
        reduceArrBy5 (
        reduceArrBy3 (
        reduceArrBy2 ({baseArr: [11, 13, 19, 23], factorArr:[]}))));


console.log (`At the end of the function chain, baseArr = ${baseArr},
    and the factorArr = ${factorArr}.`);
*/
