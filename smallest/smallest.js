/* eslint-disable indent */
/* eslint-disable no-console */
// const isNull = x => x == null;
/*
const findLargestIndex = arr => {

    const findLargestIndexRecur = (current, index) => (

        index >= arr.length
            ? current
            : arr[index] > arr[current]
            ? findLargestIndexRecur (index, index + 1)
            : findLargestIndexRecur (current, index + 1)

    );

    return findLargestIndexRecur (0, 1);

};
*/

/*
const insertDigit = (suffix, value = suffix[0], prefix = []) => (

    suffix.length === 0
        ? prefix.concat (value)
        : value < suffix[0]
        ? prefix.concat (value).concat (suffix)
        : insertDigit (suffix.slice (1), value, prefix.concat (suffix[0]))

);
*/

/*
const moveTopDown = arr => {



};
*/

/*
const findIndex = (arr, value, accum = 0) => (

    arr.length === 0 || value <= arr[0]
        ? accum
        : findIndex (arr.slice (1), value, accum + 1)

);
*/

/*
const findBottom = (arr, value) => (



);
*/

/*
const findTop = (arr, value, accum = 0) => (

    arr.length === 0 || arr[0] >= value
        ? accum
        : findTop (arr.slice (1), value, accum + 1)

);
*/
const log = console.log;

const numToArr = x => ('' + x).split('').map (x => x - 0);

const joinDigits = (x, y) => x * 10 + y;

/*
const findLowerIndex = (arr, value) => (

    arr.length === 0 || value < arr[0]
        ? 0
        : 1 + findLowerIndex (arr.slice (1), value)

);
*/

const findUpperIndex = (arr, value) => (

    arr.length === 0 || value <= arr[0]
        ? 0
        : 1 + findUpperIndex (arr.slice (1), value)

);

const findLowerIndex = (arr, i) => {

    const findLowerIndexRecur = j => {

        // log ('findLowerIndexRecur: j = ', j);

        return arr.length <= j || arr[j + 1] > arr[i]
            ? j
            : findLowerIndexRecur (j + 1);

    };

    return findLowerIndexRecur (i);

};

const findSmallestIndex = arr => {

    const findSmallestIndexRecur = (current, index) => (

        index >= arr.length
            ? current
            : arr[index] <= arr[current]
            ? findSmallestIndexRecur (index, index + 1)
            : findSmallestIndexRecur (current, index + 1)

    );

    return findSmallestIndexRecur (1, 2);

};

const findLargestIndex = arr => (

    arr.length < 2 || arr[0] > arr[1]
        ? 0
        : 1 + findLargestIndex (arr.slice (1))

);

const bumpIndex = (arr, i) => {

    const bumpIndexRecur = j => (

        j < 0
            ? 0
            : arr[j] !== arr [i]
            ? j + 1
            : bumpIndexRecur (j - 1)

    );

    return bumpIndexRecur (i - 1);

};

const isInOrder = arr => (

    arr.length < 2
        ? true
        : arr[0] > arr[1]
        ? false
        : isInOrder (arr.slice(1))

);

const moveIndexUp = (arr, oldI, newI) => (

    arr.slice (0, newI)
        .concat (arr[oldI])
        .concat (arr.slice (newI, oldI))
        .concat (arr.slice (oldI + 1))

);

const moveIndexDown = (arr, oldI, newI) => (

    arr.slice (0, oldI)
        .concat (arr.slice (oldI + 1, newI + 1))
        .concat (arr[oldI])
        .concat (arr.slice (newI + 1))

);

const moveIndex = (arr, oldI, newI) => (

    oldI < newI
        ? moveIndexDown (arr, oldI, newI)
        : oldI > newI
        ? moveIndexUp (arr, oldI, newI)
        : arr

);

const smallest = x => {

    const arrX = numToArr (x);

    if (isInOrder (arrX))
        return [x, 0, 0];

    const topDownIndex = bumpIndex (arrX, dropHead (arrX));
    const topDown = moveIndex (arrX, 0, topDownIndex)
        .reduce (joinDigits);

    const smallestIndex = bumpIndex (arrX, findSmallestIndex (arrX));
    const newSmallestIndex = findUpperIndex (arrX, arrX[smallestIndex]);

    // log ('');
    // log ('smallestIndex = ', smallestIndex);
    // log ('newSmallestIndex = ', newSmallestIndex);

    const bottomUp = moveIndex (arrX, smallestIndex, newSmallestIndex)
        .reduce (joinDigits);

    /*
    const largestIndex = findLargestIndex (arrX);
    const newLargestIndex = findBottom (arrX, arrX[largestIndex]);
    const smallestIndex = bumpIndex (arrX,
        findSmallestIndex (arrX.slice(1)) + 1);
    const newSmallestIndex = findTop (arrX, arrX[smallestIndex]);
    const topDown = moveIndex (arrX, largestIndex, newLargestIndex)
        .reduce (joinDigits);
    const bottomUp = moveIndex (arrX, smallestIndex, newSmallestIndex)
        .reduce (joinDigits);
    */
    //*
    // log ('dropHeadIndex = ', dropHeadIndex);
    // log ('topDownIndex = ', topDownIndex);
    // log ('topDown = ', topDown);
    // log ('bottomUp = ', bottomUp);
    // */

    if (topDown <= bottomUp)
        return [topDown, 0, topDownIndex];
    else
        return [bottomUp, smallestIndex, newSmallestIndex];

};

const test = (actual, expected) => (

    actual === expected
        ? log ('Test passed.')
        : log ('Test failed: ', actual, ' !== ', expected)

);

log ('Test findLargestIndex');
test (findLargestIndex (numToArr (2015874)), 0);
test (findLargestIndex (numToArr (123421353)), 3);
test (findLargestIndex (numToArr (234445)), 5);
test (findLargestIndex (numToArr (43556)), 0);
test (findLargestIndex (numToArr (1111171)), 5);
log ();

log ('Test findSmallestIndex');
test (findSmallestIndex (numToArr (1234567)), 1);
test (findSmallestIndex (numToArr (1111011)), 4);
test (findSmallestIndex (numToArr (123432343223)), 10);
test (findSmallestIndex (numToArr (700234)), 2);
test (findSmallestIndex (numToArr (7002304)), 5);
test (findSmallestIndex (numToArr (1234512)), 5);
log ();

log ('Test findUpperIndex');
test (findUpperIndex (numToArr (1234567), 4), 3);
test (findUpperIndex (numToArr (33444555666), 4), 2);
test (findUpperIndex (numToArr (70012), 6), 0);
test (findUpperIndex (numToArr (111333555), 2), 3);
log ();

log ('Test findLowerIndex');
test (findLowerIndex (numToArr (1234567), 4), 3);
test (findLowerIndex (numToArr (33444333666), 4), 8);
test (findLowerIndex (numToArr (33444333166), 4), 9);
test (findLowerIndex (numToArr (70012), 6), 0);
test (findLowerIndex (numToArr (111333555), 2), 3);
log ();

log (smallest (261235)); // [126235, 2, 0]);
/*
log (smallest (209917)); // [29917, 0, 1]);
log (smallest (285365)); // [238565, 3, 1]);
log (smallest (269045)); // [26945, 3, 0]);
log (smallest (296837)); // [239687, 4, 1]);
// */

// log (smallest (7168843238239138)); // [1678843238231938, 0, 2]
