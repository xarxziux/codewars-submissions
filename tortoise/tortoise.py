from math import floor

def race(v1, v2, g):

    if v2 <= v1:
        return None

    total_seconds = floor ((g * 3600)  / (v2 - v1))
    seconds = total_seconds % 60
    total_minutes = (total_seconds - seconds) / 60
    minutes = total_minutes % 60
    hours = (total_minutes - minutes) / 60

    return [hours, minutes, seconds]
