fn race(v1: i32, v2: i32, g: i32) -> Option<Vec<i32>> {
  
  if v1 >= v2 {return None};
  
  let total_seconds: i32 = ((g as f64 * 3600.0) / (v2 - v1) as f64).floor() as i32;
  let seconds: i32 = total_seconds % 60;
  let total_minutes: i32 = (total_seconds - seconds) / 60;
  let minutes: i32 = total_minutes % 60;
  let hours: i32 = (total_minutes - minutes) / 60;
  
  Some (vec! [hours, minutes, seconds])
  
}
