function race(v1, v2, g) {

    if (v1 >= v2)
        return null;

    const total_seconds = Math.floor ((g * 3600)  / (v2 - v1));
    const seconds = total_seconds % 60;
    const total_minutes = (total_seconds - seconds) / 60;
    const minutes = total_minutes % 60;
    const hours = (total_minutes - minutes) / 60;

    return [hours, minutes, seconds];

}

module.exports = race;
