dist <- function (v, mu) {
  distMS (v*5/18, mu)
}

distMS <-function (v, mu) {
  v + (v*v) / (mu * 19.62)
}
