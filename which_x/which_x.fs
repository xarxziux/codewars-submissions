open System

let getSum (x: float): float =
  let rec gsInt (i: int) (a: float): float =
    //match x >= 1.0 with
    //| true -> 2.305843007e+18
    //| false ->
      let c = (float i) * (x ** (float i))
      match c < 1e-10 || c > 1000000.0 with
      | true -> a
      | false -> gsInt (i + 1) (a + c)
  gsInt 1 0.0

let getInc (o: int): float =
  1.0 / (10.0 ** float o)

let solve (t: float): float =
  let rec solveInt (c: float) (o: int): float =
    //printfn "c - 0 = %.20f - %i" c o
    let inter: float = getSum c
    //printfn "  inter = %f" inter
    match abs (inter - t) < 1e-12 || o > 15 with
    | true -> c
    | false ->
      let o': float = getInc o
      //printfn "    o' = %f" o'
      let c': float = c + o'
      //printfn "    c' = %.20f" c'
      let n: float = getSum c'
      //printfn "    n = %f" n
      match n - t > 1e-10 with
      | true -> solveInt c (o + 1)
      | false -> solveInt c' o
  solveInt 0.0 1
