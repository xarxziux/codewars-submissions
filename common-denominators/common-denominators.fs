let rec gcd (n1: int) (n2: int): int =
    if n1 = n2 then n1
    else if n1 >= n2 then gcd (n1 - n2) n2
    else gcd n1 (n2 - n1)

let convertFracts (ls: (int * int) list): (int * int) list =
    let newD: int =
        ls
        |> List.map (fun (x, y) -> (x / (gcd x y)), (y / (gcd x y)))
        |> List.map (fun (_, x) -> x)
        |> List.fold (fun n1 n2 -> n1 * n2 / gcd n1 n2) 1
    List.map (fun (n, d) -> (n * newD / d), newD) ls
