module Codewars.Kata.CommonDenominators where

type Ratio a = (a, a) -- Data.Ratio not suitable for this kata

simplifyFract :: Integral a => Ratio a -> Ratio a
simplifyFract (x, y) = (quot x g, quot y g)
    where g = gcd x y

getLCM :: Integral a => a -> a -> a
getLCM x y = quot (x * y) (gcd x y)

getMult :: Integral a => [Ratio a] -> a
getMult xs = foldl getLCM 1 . map snd $ map simplifyFract xs

raiseByMult :: Integral a => a -> Ratio a -> Ratio a
raiseByMult m (x, y) = (quot (m * x) y, m)

convertFracs :: Integral a => [Ratio a] -> [Ratio a]
convertFracs xs = map (raiseByMult m) xs
    where m = getMult xs
