const factorial  = x => (

    function factorialRecur (x, accum) {

        return ((x < 2)
            ? accum
            : factorialRecur (x - 1, accum * x));

    }(x, 1)

);

const pascal = (row, elem) => (

    factorial (row) / (factorial (elem) * factorial (row - elem))

);

const diagonal = (x, y) => pascal (x + 1, y + 1);

module.exports = diagonal;
