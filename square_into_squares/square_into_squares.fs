let lowerSquare (x: int64): int64 =
  float x |> sqrt |> int64

let sumSquares (xs: int64 list): int64 =
  List.map (fun x -> x * x) xs |> List.sum

let rec decrementAccum (acc: int64 list): int64 list =
  match List.length acc < 2 with
  | true -> []
  | false ->
    match acc.[1] - acc.[0] > 1L with
    | true -> (acc.[1] - 1L) :: acc.[2 .. ]
    | false -> decrementAccum acc.[1 .. ]

let rec getBreakdown (acc: int64 list, rem: int64): int64 list * int64 =
  if rem <= 0L || acc.[0] = 1L
    then (acc, rem)
  else
    let x: int64 = List.min [lowerSquare rem; acc.[0] - 1L]
    getBreakdown (x :: acc, rem - x * x)

let decompose (x: int64) =
  let rec callBreakdown (acc: int64 list, rem: int64): int64 list =
    if rem = 0L || List.length acc = 0 then acc
    else
      let acc' = decrementAccum acc
      if acc' = [] then []
      else getBreakdown (acc', x * x - (sumSquares acc')) |> callBreakdown
  let ans =
    getBreakdown ([x - 1L], x * 2L - 1L)
    |> callBreakdown
    |> List.map (fun x -> x.ToString())
    |> List.toSeq
    |> String.concat " "
  if ans = "" then null
  else ans
