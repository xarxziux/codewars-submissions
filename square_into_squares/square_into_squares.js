const lowerSquare = x => Math.floor (Math.sqrt (x));
const sumSquares = arr => arr.reduce ((a, x) => a + (x * x), 0);

const decrementAccum = (arr) => (

    arr.length < 2
        ? []
        : arr[1] - arr[0] > 1
            ? [arr[1] - 1, ...arr.slice (2)]
            : decrementAccum (arr.slice (1))

);

const getBreakdown = ({accum = [], remain}) => {

    if (remain <= 0 || accum[0] === 1)
        return {accum, remain};

    const nextValue = Math.min (lowerSquare (remain), accum[0] - 1);

    return getBreakdown ({
        accum: [nextValue, ...accum],
        remain: remain - (nextValue * nextValue)
    });

};

const decompose = x => {

    let {accum, remain} = getBreakdown ({accum: [x - 1], remain: x * 2 - 1});

    while (remain !== 0 && accum.length > 0) {

        const newAccum = decrementAccum (accum);

        if (newAccum.length === 0)
            return null;

        ({accum, remain} = getBreakdown ({
            accum: newAccum,
            remain: x * x - sumSquares (newAccum)
        }));

    }

    return accum;

};

module.exports = decompose;
