const tickets = peopleInLine => {

    const cd = cashDrawer();
    let canSell = true;
    let nextInLine = 0;

    while ((nextInLine < peopleInLine.length) && (canSell)) {

        canSell = cd(peopleInLine[nextInLine]);
        nextInLine++;

    }

    return ((canSell)
        ? 'YES'
        : 'NO');

};

const cashDrawer = () => {

    let c25 = 0;
    let c50 = 0;

    return payment => {

        if (payment === 25) {

            c25++;
            return true;

        }

        if (payment === 50) {

            if (c25 > 0) {

                c50++;
                c25--;
                return true;

            } else {

                return false;

            }

        }

        if (payment === 100) {

            if ((c50 > 0) && (c25 > 0)) {

                c25--;
                c50--;
                return true;

            } else if (c25 > 2) {

                c25 = c25 - 3;
                return true;

            } else {

                return false;

            }

        }

    };

};

module.exports = tickets;
