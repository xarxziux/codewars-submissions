const latinize = str => (
    str.length < 2
        ? str
        : str.slice (1) + str[0] + 'ay'
);

const isCharacter = asc => (
    ((asc > 64) && (asc < 91)) || ((asc > 96) && (asc < 123))
);

const parseStr = (head, tail) => (

    tail === ''
        ? latinize (head)
        : isCharacter (tail.charCodeAt())
        ? parseStr (head + tail [0], tail.slice (1))
        : latinize (head) + tail[0] + parseStr ('', tail.slice(1))

);

const pigIt = str => (
    parseStr ('', str)
);

module.exports = pigIt;
