fn solution(num: i32) -> i32 {
  let mut i: i32 = 0;
  let mut accum: i32 = 0;
  while i < num {
    if (i % 3 == 0) || (i % 5 == 0) {
      accum = accum + i;
    }
    
    i = i + 1;
    
  }
  
  accum
  
}
