function solution (number){

  return sumValsRecur (0, number, 0);

}

const isDivisibleBy = base => val => (

    val % base === 0

);

const isMultipleOf3 = isDivisibleBy (3);
const isMultipleOf5 = isDivisibleBy (5);
const isMultipleOf3Or5 = val =>
  isMultipleOf3 (val) || isMultipleOf5 (val);

const sumValsRecur = (current, max, accum) => (

  (current >= max)
    ? accum
    : isMultipleOf3Or5 (current)
    ? sumValsRecur (current + 1, max, accum + current)
    : sumValsRecur (current + 1, max, accum)

);

const sumValsRecur = (current, max, accum) => (

    (current >= max)
        ? accum
        : isMultipleOf3Or5 (current)
            ? sumValsRecur (current + 1, max, accum + current)
            : sumValsRecur (current + 1, max, accum)

);

module.exports = solution;
