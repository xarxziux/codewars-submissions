const isNull = x => x == null;

const initStack = () => {

    const stack = [];

    const isEmpty = () => stack.length === 0;

    const peek = () => {

        if (stack.length === 0)
            return null;

        return stack [stack.length - 1];

    };

    const pop = () => {

        if (stack.length === 0)
            throw new Error ('Illegal pop() operation on the base of a stack.');

        return stack.pop();

    };

    const push = x => {

        stack.push (x);

    };

    return {
        isEmpty,
        peek,
        pop,
        push
    };

};

let props = {};
let prev = initStack();
let next = initStack();

const get = x => props[x];

const set = (key, value) => {

    prev.push (props);

    if (!next.isEmpty)
        next = initStack();

    props = Object.assign ({}, props, {[key]: value});

};

const del = key => {

    prev.push (props);

    if (!next.isEmpty())
        next = initStack();

    props = Object.assign ({}, props, {[key]: undefined});

};

const undo = () => {

    if (prev.isEmpty())
        throw new Error ('Error: undo stack empty');

    next.push (props);
    props = prev.pop();

};

const redo = () => {

    if (next.isEmpty())
        throw new Error ('Error: redo stack empty');

    prev.push (props);
    props = next.pop();

};

const undoRedo = () => {
    return null;
};

module.exports = {
    get,
    set,
    del,
    undo,
    redo,
    undoRedo
};
