const raiseError = message => {
    
    throw new Error (message);
    
};

const undoRedo = mainObj => {
    
    const stack = [];
    stack[0] = mainObj;
    let stackPointer = 0;
    
    const get = key => stack[stackPointer][key];
    
    const set = (key, value) => {
        
        stackPointer = stackPointer + 1;
        stack.length = stackPointer;
        
        stack.push (Object.assign (
            {}, stack[stackPointer - 1], {[key]: value}));
        
        mainObj[key] = value;
        
    };
    
    const del = key => {
        
        stackPointer = stackPointer + 1;
        stack.length = stackPointer;
        
        stack.push (Object.assign ({}, stack[stackPointer - 1]));
        
        delete stack[stackPointer][key];
        mainObj = stack[stackPointer];
        
    };
    
    const undo = () => {
        
        if (stackPointer < 1)
            raiseError ('Undo stack empty');
        
        stackPointer = stackPointer - 1;
        mainObj = stack[stackPointer];
        
    };
    
    const redo = () => {
        
        if (stackPointer === stack.length - 1)
            raiseError ('Redo stack empty');
        
        stackPointer = stackPointer + 1;
        mainObj = stack[stackPointer];
        
    };
    
    return {
        get,
        set,
        del,
        undo,
        redo
    };
    
};
