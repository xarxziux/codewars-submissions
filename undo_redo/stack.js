const initStack = () => {

    const stack = [];

    const isEmpty = () => stack.length === 0;

    const peek = () => {

        if (stack.length === 0)
            return null;

        return stack [stack.length - 1];

    };

    const pop = () => {

        if (stack.length === 0)
            throw new Error ('Illegal pop() operation on the base of a stack.');

        return stack.pop();

    };

    const push = x => {

        stack.push (x);

    };

    return {
        isEmpty,
        peek,
        pop,
        push
    };

};

module.exports = initStack;
