const undoRedo = mainObj => {

    const stack = [];
    let stackPointer = -1;

    const unCreate = () => {

        delete mainObj[stack[stackPointer].keyName];

        stackPointer = stackPointer - 1;

    };

    const unUpdate = () => {

        mainObj [stack[stackPointer].keyName] =
            stack[stackPointer].oldValue;

        stackPointer = stackPointer - 1;

    };

    const reCreate = () => {

        stackPointer = stackPointer + 1;

        mainObj [stack[stackPointer].keyName] =
            stack[stackPointer].newValue;

    };

    const reDelete = () => {

        stackPointer = stackPointer + 1;

        delete mainObj [stack[stackPointer].keyName];

    };

    const get = key => mainObj[key];

    const set = (key, value) => {

        const isNewProp = !mainObj.hasOwnProperty (key);
        stackPointer = stackPointer + 1;
        stack.length = stackPointer + 1;

        stack[stackPointer] = {
            action: (isNewProp
                ? 'create'
                : 'update'),
            keyName: key,
            newValue: value,
            oldValue: (isNewProp
                ? null
                : mainObj[key])

        };

        mainObj[key] = value;

    };

    const del = key => {

        if (!mainObj.hasOwnProperty (key))
            return null;

        stackPointer = stackPointer + 1;
        stack.length = stackPointer + 1;

        stack[stackPointer] = {
            action: 'delete',
            keyName: key,
            newValue: null,
            oldValue: mainObj[key]

        };

        delete mainObj[key];

    };

    const undo = () => {

        if (stackPointer < 0)
            throw new Error ('Illegal undo() call on an empty stack');

        if (stack[stackPointer].action === 'create')
            unCreate();
        else
            unUpdate();

    };

    const redo = () => {

        if (stack.length <= stackPointer + 1)
            throw new Error ('Illegal redo() call on an empty stack');

        if (stack[stackPointer + 1].action === 'delete')
            reDelete();
        else
            reCreate();

    };

    return {
        get,
        set,
        del,
        undo,
        redo,
    };

};

module.exports = undoRedo;
