comp <- function(a1, a2) {
  if (length (a1) == 0 || length (a2) == 0 || length (a1) != length (a2)) {
    FALSE
  } else {
    a3 = (sort (a1) ^ 2) - (sort (a2))
    length (a3[a3 != 0]) == 0
   }
}
