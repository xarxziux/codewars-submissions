fn get_score (counts: &str, is_good: bool) -> i32 {
    
    let base_scores: Vec <i32> = match is_good {
        true => vec![1, 2, 3, 3, 4, 10],
        false => vec![1, 2, 2, 2, 3, 5, 10]
    };
    
    counts.split (' ')
        .zip (base_scores.iter())
        .fold (0, |accum: i32, x: (&str, &i32)|
                accum + (x.0.parse::<i32>().unwrap() * x.1))
}

fn good_vs_evil(good: &str, evil: &str) -> String {
    
    let good_score = get_score (good, true);
    let evil_score = get_score (evil, false);
    
    if good_score > evil_score {
        "Battle Result: Good triumphs over Evil".to_string()
    } else if good_score < evil_score {
        "Battle Result: Evil eradicates all trace of Good".to_string()
    } else {
        "Battle Result: No victor on this battle field".to_string()
    }
}
