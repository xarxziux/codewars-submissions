def getBase (isGood):
    if isGood:
        return [1, 2, 3, 3, 4, 10]
    return [1, 2, 2, 2, 3, 5, 10]

def getScore (scores, isGood):
    base = getBase (isGood)
    scoreList = scores.split (' ')
    i = 0
    iMax = len (scoreList)
    accum = 0

    while i < iMax:
        accum = accum + (int (scoreList [i]) * base [i])
        i = i + 1

    return accum

def goodVsEvil(good, evil):
    goodScore = getScore (good, True)
    evilScore = getScore (evil, False)

    if (goodScore > evilScore):
        return "Battle Result: Good triumphs over Evil"

    if (goodScore < evilScore):
        return "Battle Result: Evil eradicates all trace of Good"

    return "Battle Result: No victor on this battle field"
