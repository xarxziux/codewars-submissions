const getRightEdge = matrix => (

    function getRightEdgeRecur (matrix, accum) {

        return ((matrix.length === 0)
            ? accum
            : getRightEdgeRecur (matrix.slice (1),
                accum.concat (matrix[0][matrix[0].length - 1])));

    }(matrix, [])

);

const getLeftEdge = matrix => (

    function getLeftEdgeRecur (matrix, accum) {

        return ((matrix.length === 0)
            ? accum
            : getLeftEdgeRecur (matrix.slice (0, -1),
                accum.concat (matrix[matrix.length - 1][0])));

    }(matrix, [])

);

const topAndTail = matrix => (

    function topAndTailRecur (matrix, accum) {

        return ((matrix.length === 0)
            ? accum
            : topAndTailRecur (matrix.slice (1),
                accum.concat ([matrix[0].slice (1, -1)])));

    }(matrix, [])

);

const trimEdge = matrix => (

    (matrix.length < 3)
        ? []
        : topAndTail (matrix.slice (1, -1))

);

const getEdges = matrix => (

    function getEdgeRecur ({matrix, trail}) {

        console.log (`matrix = ${matrix}, trail = ${trail}`);

        return ((matrix.length === 0)
            ? trail
            : (matrix.length === 1)
                ? trail.concat (matrix [0][0])
                : getEdgeRecur ({
                    matrix: trimEdge (matrix),
                    trail: trail.concat (matrix [0]
                        .concat (getRightEdge (matrix.slice (1)))
                        .concat (matrix [matrix.length - 1].reverse().slice(1))
                        .concat (getLeftEdge (matrix.slice (1, -1))))

                })

        );

    }({matrix, trail: []})

);

const snail = matrix => (

    (matrix [0].length === 0)
        ? []
        : getEdges (matrix)

);

module.export = snail;
