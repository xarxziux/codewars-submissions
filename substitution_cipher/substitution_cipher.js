const encipher = codebook => char => (

    codebook [char] || char

);

function SubstitutionCipher (raw, code) {

    const rawValues = raw.split ('');
    const codeValues = code.split ('');

    const rawObj = {};
    const codeObj = {};

    let i = 0;

    while (i < rawValues.length) {

        rawObj [rawValues[i]] = codeValues[i];
        codeObj [codeValues[i]] = rawValues[i];

        i = i + 1;

    }

    this.encode = str => (

        str.split ('').map (encipher (rawObj)).join ('')

    );

    this.decode = str => (

        str.split ('').map (encipher (codeObj)).join ('')

    );

}

var abc1 = 'abc';
var abc2 = 'bca';
// var abc1 = "abcdefghijklmnopqrstuvwxyz";
// var abc2 = "etaoinshrdlucmfwypvbgkjqxz";

var sub = new SubstitutionCipher(abc1, abc2);
console.log (sub.encode ('abcdef'));
console.log (sub.decode ('abcdef'));

/*
console.log (sub.encode("abc")); // => "eta"
console.log (sub.encode("xyz")); // => "qxz"
console.log (sub.encode("aeiou")); // => "eirfg"

console.log (sub.decode("eta")); // => "abc"
console.log (sub.decode("qxz")); // => "xyz"
console.log (sub.decode("eirfg")); // => "aeiou"
*/
