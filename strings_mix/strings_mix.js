const isNotNull = x => x != null;

const initArray = x => Array (x).fill (0);

const oneToZero = x => x === 1 ? 0 : x;

const incIndex = (arr, index) => (

    index < 0 || index >= arr.length
        ? arr
        : arr.slice (0, index)
            .concat (arr[index] + 1)
            .concat (arr.slice (index + 1))

);

const countChars = (str, accum = initArray (26)) => {

    // console.log ('countChars: accum = ' + accum);

    if (str === '')
        return accum.map (oneToZero);

    const nextChar = str.charCodeAt() - 97;
    const isLowerCase = nextChar >= 0 && nextChar < 26;

    return countChars (
        str.slice(1),
        isLowerCase
            ? incIndex (accum, nextChar)
            : accum
    );

};

const combineArrs = (arr1, arr2, accum = []) => (

    arr1.length === 0 && arr2.length === 0
        ? accum
        : combineArrs (
            arr1.slice (1),
            arr2.slice (1),
            accum.concat ([[arr1[0], arr2[0]]])
        )

);

const combineCounts = (elem, index) => (

    elem[0] === 0 && elem[1] === 0
        ? null
        : {
            char: String.fromCharCode (index + 97),
            prefix: (elem[0] > elem[1]
                ? '1:'
                : elem[0] < elem[1]
                    ? '2:'
                    : '=:'),
            count: Math.max (elem[0], elem[1])
        }

);

const sortItems = (a, b) => (

    a.count !== b.count
        ? b.count - a.count
        : a.prefix !== b.prefix
            ? a.prefix.charCodeAt() - b.prefix.charCodeAt()
            : a.char.charCodeAt() - b.char.charCodeAt()

);

const sortBreakdown = arr => (

    arr.slice().sort (sortItems)

);

const breakdownToString = x => (

    x.prefix + x.char.repeat (x.count)

);

const mix = (str1, str2) => (

    sortBreakdown (combineArrs (
        countChars (str1),
        countChars (str2))
        .map (combineCounts)
        .filter (isNotNull))
        .map (breakdownToString)
        .join ('/')

);

module.exports = mix;
