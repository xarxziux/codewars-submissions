open System

let countChars (s: string): Map<char, int> =
  let rec countCharsInt (s: string) (acc: Map<char, int>): Map<char, int> =
    match s with
    | "" -> Map.filter (fun _ x -> x > 1) acc
    | _ ->
      let c: char = s.[0]
      match Char.IsLower c with
      | false -> countCharsInt s.[ 1 .. ] acc
      | true ->
        match Map.containsKey c acc with
        | true -> countCharsInt s.[ 1 .. ] (Map.add c (acc.[c] + 1) acc)
        | false -> countCharsInt s.[ 1 .. ] (Map.add c 1 acc)
  countCharsInt s Map.empty

let getPrefix (c: char) (x1: int) (x2: int): char * string * int =
  if x1 > x2 then (c, "1:", x1)
  else if x1 < x2 then (c, "2:", x2)
  else (c, "=:", x1)

let combineCounts (m1: Map<char, int>) (m2: Map<char, int>): (char * string * int) list =
  let rec ccInt (x: int) (acc: (char * string * int) list): (char * string * int) list =
    match x > 25 with
    | true -> acc
    | false ->
      let c = char (x + 97)
      match (Map.containsKey c m1, Map.containsKey c m2) with
      | (true, false) -> ccInt (x + 1) ((c, "1:", m1.[c]) :: acc)
      | (false, true) -> ccInt (x + 1) ((c, "2:", m2.[c]) :: acc)
      | (false, false) -> ccInt (x + 1) acc
      | (true, true) -> ccInt (x + 1) (getPrefix c m1.[c] m2.[c] :: acc)
  ccInt 0 List.empty

let sortBreakdown (xs: (char * string * int) list): (char * string * int) list =
  let sortItems (ch1: char, pr1: string, cnt1: int) (ch2: char, pr2: string, cnt2: int) =
    if cnt1 <> cnt2 then cnt2 - cnt1
    else if pr1 <> pr2 then (int pr1.[0]) - (int pr2.[0])
    else (int ch1) - (int ch2)
  List.sortWith sortItems xs

let breakdownToString (c: char, s: string, i: int): string =
  String.concat "" [| s; String.replicate i (System.String [| c |]) |]

let mix (s1: string) (s2: string): string =
  combineCounts (countChars s1) (countChars s2)
    |> sortBreakdown
    |> List.map breakdownToString
    |> List.toSeq
    |> String.concat "/"
