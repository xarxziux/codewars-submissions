use std::cmp::Ordering;
use std::iter;

fn main() {
    
    println! ("The final answer is {}.",
        // mix (&"Hello World!".to_string(), &"I'm calling for you.".to_string()));
        mix ("Hello World!", "I'm calling for you."));
    
}

fn mix (str1: &str, str2: &str) -> String {
    
    // let str1: String = "Hello World!".to_string();
    // let str2: String = "I'm calling for you.".to_string();
    // let breakdown: Vec<(char, char, u32)> = combine_arr (
    //         string_to_count (str1.to_string()),
    //         string_to_count (str2.to_string()));
    
    //println! ("Breakdown = {:?}.", breakdown);
    
    // let sorted_breakdown: Vec<(char, char, u32)> = sort_vector (breakdown);

    //println! ("Sorted breakdown = {:?}.", sorted_breakdown);
    
    // vector_to_string (&sorted_breakdown)
    // vector_to_string (&sort_vector (breakdown))
    // let string_breakdown: String = vector_to_string (&sorted_breakdown);
    
    //println! ("Final result = {:?}.", string_breakdown);
    
    // string_breakdown
    
    vector_to_string (&sort_vector (combine_arr (
            string_to_count (str1.to_string()),
            string_to_count (str2.to_string()))))
    
}

fn one_to_zero (x: &u32) -> u32 {
    
    match *x {
        1 => 0,
        _ => *x
    }
    
}

fn get_lowercase (in_str: String) -> Vec<u32> {
    
    in_str.chars().filter (is_lower_case).map (char_to_index).collect()
    
}

fn is_lower_case (c: &char) -> bool {
    
    c.is_lowercase()
    
}

fn char_to_index (c: char) -> u32 {
    
    (c as u32) - 97
    
}

fn count_chars (index_vec: Vec<u32>) -> [u32; 26] {
    
    let mut count_arr: [u32; 26] = [0; 26];
    let mut j = 0;
    
    for i in index_vec {
        let j: usize = i as usize;
        count_arr[j] = count_arr[j] + 1;
    }
    
    while j < 26 {
        count_arr[j] = one_to_zero (&count_arr[j]);
        j = j + 1;
    }
    
    count_arr
    
}

fn string_to_count (in_str: String) -> [u32; 26] {
    
    count_chars (get_lowercase (in_str))
    
}

fn element_to_tuple (elem: (usize, (&u32, &u32))) -> (char, char, u32) {
    
    if *(elem.1).0 == 0 && *(elem.1).1 == 0 {
        ('!', '!', 0)
    } else {(
        ((elem.0 + 97) as u8) as char,
        if (elem.1).0 > (elem.1).1 {
            '1'
        } else if (elem.1).0 < (elem.1).1 {
            '2'
        } else {
            '='
        },
        std::cmp::max (*(elem.1).0, *(elem.1).1)
    )}
    
}

fn is_empty (elem: &(char, char, u32)) -> bool {
    
    elem.0 != '!'
    
}

fn combine_arr (arr1: [u32; 26], arr2: [u32; 26]) -> Vec<(char, char, u32)> {
    
    arr1.iter()
        .zip (arr2.iter())
        .enumerate()
        .map (element_to_tuple)
        .filter (is_empty)
        .collect()
    
}

fn sort_tuple (t1: &(char, char, u32), t2: &(char, char, u32)) -> Ordering {
    
    if t1.2 > t2.2 {
        Ordering::Less
    } else if t1.2 < t2.2 {
        Ordering::Greater
    } else if (t1.1 as u32) < (t1.1 as u32) {
        Ordering::Less
    } else if (t1.1 as u32) > (t1.1 as u32) {
        Ordering::Greater
    } else if (t1.0 as u32) < (t1.0 as u32) {
        Ordering::Less
    } else {
        Ordering::Greater
    }
    
}

fn sort_vector (in_vec: Vec<(char, char, u32)>) -> Vec<(char, char, u32)> {
    
    let mut out_vec = in_vec.clone();
    out_vec.sort_by(sort_tuple);
    out_vec
    
}

fn tuple_to_string (in_tup: &(char, char, u32)) -> String {
    
    let mut out_str: String = String::new();
    out_str.push (in_tup.1);
    out_str.push (':');
    out_str.push_str (&iter::repeat (in_tup.0.to_string()).take (in_tup.2 as usize).collect::<Vec<String>>().join(""));
    out_str
    
}

fn vector_to_string (in_vec: &Vec<(char, char, u32)>) -> String {
    
    let mut out_str: String = String::new();
    in_vec.iter().map(tuple_to_string).collect::<Vec<String>>().join("/")
    
}

