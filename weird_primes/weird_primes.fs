let rec gcd (n1: int) (n2: int): int =
  if n1 = n2 then n1
  else if n1 >= n2 then gcd (n1 - n2) n2
  else gcd n1 (n2 - n1)

let rec a (x: int): int =
  match x with
  | 0 -> 6
  | _ ->
    let y = a (x - 1)
    y + gcd x y

let rec an (x: int): int list =
  match x with
  | 0 -> [a 0]
  | _ -> [a x] |> List.append (an (x - 1))

let g (x: int): int =
  match x > 0 with
  | false -> failwith "Invalid parameter"
  | true -> x - 1 |> a |> (-) (a x)

let rec gn (x: int): int list =
  match x with
  | y when y < 1 -> failwith "Invalid parameter"
  | y when y = 1 -> [1]
  | _ -> List.append (gn (x - 1)) [(a x) - (a (x - 1))]

let countOnes (x: int): int =
  gn x |> List.filter (fun x -> x = 1) |> List.sum

let rec nextPrime (x: int): int * int =
  let y = g x
  match y with
  | 1 -> nextPrime (x + 1)
  | _ -> x, y

let rec nextPrimes (t: int) (c: int) (a: Set<int>): Set<int> =
  if t <= a.Count then a
  else
    let n = nextPrime c
    nextPrimes t (fst n + 1) (a.Add (snd n))

let p (x: int): int list =
  nextPrimes x 1 Set.empty |> Set.toList

let maxPn (x: int): int =
  p x |> List.max

(*
let anOver (x: int) =
  gn x
    |> List.mapi (fun x y -> x + 1, y)
    |> List.filter (fun (_, y) -> y <> 1)

let rec anBuild (t: int) (c: int) (a: Set<int * int>) =
  if t <= a.Count then a
  else
    let n = nextPrime c
    anBuild t (fst n + 1) (a.Add n)
*)

let rec anBuild (t: int) (c: int) (p: Set<int>) (a: float list): float list =
  if t <= p.Count then a
  else
    let n = nextPrime c
    match p.Contains (snd n) with
    | true -> anBuild t (fst n + 1) p a
    | false -> anBuild t (fst n + 1) (p.Add (snd n)) ((float (snd n) / float (fst n)) :: a)

let anOver (x: int): float list =
  anBuild x 1 Set.empty []

let anOverAverage (x: int): int =
  let xs = anOver x
  List.length xs
    |> float
    |> (/) (List.sum xs)
    |> round
    |> int
