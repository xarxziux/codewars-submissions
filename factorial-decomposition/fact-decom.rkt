#lang racket
(provide decomp)

(define (get-twos nums)
  (let
      ([fst (car nums)]
       [rst (cdr nums)])
    (cond
      [(= fst 1) rst]
      [(even? fst) (get-twos (cons (/ fst 2) (cons 2 rst)))]
      [else nums])))

(define (get-odd-factors nums [fact 3])
  (let
      ([fst (car nums)]
       [rst (cdr nums)])
    (cond
      [(= fst 1) rst] 
      [(> (* fact 2) fst) nums]
      [(= 0 (remainder fst fact))
       (get-odd-factors (cons (/ fst fact) (cons fact rst)) fact)]
      [else (get-odd-factors nums (+ fact 2))])))

(define (get-all-factors nums)
  (get-odd-factors (get-twos nums)))

(define (list->factors nums)
  (if
   (null? nums)
   nums
   (sort
    (get-all-factors (cons (car nums) (list->factors (cdr nums))))
    <)))

(define (range->factors n)
  (list->factors (range 2 (+ n 1))))

(define (stringify-count num count)
  (string-append
   (number->string num)
   (if (= count 1) "" (string-append "^" (number->string count)))))

(define (count-head nums [num (car nums)] [count 0])
  (cond
    [(null? nums) (stringify-count num count)]
    [(= (car nums) num) (count-head (cdr nums) num (+ count 1))]
    [else
     (string-append
      (stringify-count num count)
      " * "
      (count-head (cdr nums) (car nums) 1))]))

(define (decomp n)
  (count-head (range->factors n)))

