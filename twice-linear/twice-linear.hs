module TwiceLinear (dblLinear) where

import qualified Data.Set as Set

setLimit :: Integer
setLimit = 2000000

addItem :: Integer -> Set.Set Integer -> Set.Set Integer
addItem x xs
    | x > setLimit = xs
    | otherwise = addItem z . addItem y $ Set.insert x xs
        where
            y = x * 2 + 1
            z = x * 3 + 1

dlSeq :: Set.Set Integer
dlSeq = addItem 1 Set.empty

dblLinear :: Int -> Integer
dblLinear x = (Set.toList dlSeq) !! x
