let setLimit: int = 250000

let rec addItem (x: int) (acc: Set<int>): Set<int> =
  match x > setLimit with
  | true -> acc
  | false ->
    acc
      |> Set.add x
      |> addItem (x * 2 + 1)
      |> addItem (x * 3 + 1)

let dlSeq: int list =
  addItem 1 Set.empty
    |> Set.toList
    |> List.sort

let dblLinear (x: int): int =
  if x > List.length dlSeq then 0
  else dlSeq.[x]
