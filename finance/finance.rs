fn finance (n: u64) -> u64 {
    
    let mut accum: u64 = 0;
    let mut x: u64 = n;
    
    while x > 0 {
        
        accum = accum + ((3 * x * x + 3 * x) / 2);
        x = x - 1;
        
    }
    
    accum
    
}
