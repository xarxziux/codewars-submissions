let finance = x => (
    
    x === 0
        ? 0
        : ((3 * x * x + 3 * x) / 2) + finance (x - 1)
    
);
