const stripZero = (x = '') => (

    (x.length <= 1 || x[0] !== '0'
        ? x
        : stripZero (x.slice(1)))

);

const breakdown = x => ({

    carry: (x < 10 ? '' : '' + Math.floor (x / 10)),
    result: '' + x % 10

});

const multiplyStringDigits = (x, y, carry) => (

    breakdown ((x - 0) * (y - 0) + (carry - 0))

);

const stringMult = (value, digit, accum = '', carry = '') => {

    if (value === '')
        return carry + accum;

    const nextCalc = multiplyStringDigits (value.slice (-1), digit, carry);

    return stringMult (

        value.slice (0, -1),
        digit,
        nextCalc.result + accum,
        nextCalc.carry

    );

};

const stringDigitAdd = (x = '', y = '', carry = '') => (

    breakdown ((x -0) + (y - 0) + (carry - 0))

);

const stringAdd = (x, y, accum = '', carry = '') => {

    if ((x === '') && (y === '') && (carry === ''))
        return accum

    const nextCalc = stringDigitAdd (x.slice (-1), y.slice (-1), carry);

    return stringAdd (
        x.slice (0, -1),
        y.slice (0, -1),
        nextCalc.result + accum,
        nextCalc.carry
    );

};

const longMult = (x, y) => (

    y.length === 0
        ? ''
        : stringAdd (
            stringMult (x, y.slice(-1)),
            longMult (x + '0', y.slice (0, -1)))

);

const multiply = (x, y) => (

    stripZero (longMult (x, y))

);

module.exports = multiply;
